#!/bin/bash

input=$1
output=$2

# USAGE -> scanickel dossier fichier.pdf
# REQUIREMENTS -> imagemagick, tesseract, image2pdf, ocrmypdf, figlet

#-----------#
# VARIABLES #
#-----------#

# Color variables
red='\033[0;31m'
green='\033[0;32m'
# yellow='\033[0;33m'
magenta='\033[0;35m'
blue='\033[0;36m'

# Clear the color after that
clear='\033[0m'

# Options variables
declare IM_BONUS=""
declare IMG_BONUS=""
declare PDF_BONUS=""
declare TES_BONUS=""

### FONCTIONS ###

fn_bye() { echo "Aurevoir!"; exit 0; }
fn_fail() { echo -e "${red}Option invalide${clear}"; }

options_sup() {
  local _outvar=$1
  local _result

  echo -ne "  RAJOUTER DES OPTIONS POUR ${1} (o/N) ? -> "
  read -r answer
  case $answer in
    o | O)
      echo -ne "${blue}Écris les ajouts ici: ${clear}"
      read -r BONUS
      eval "$2='$BONUS'"
      ;;
    n | N | "")
      echo "Rien de spécial"
      ;;
    *)
      fn_fail
      options_sup $1
      ;;
  esac

  eval $_outvar=\$_result
}

####################### DÉMARRAGE ########################
if [ "${2##*.}" != 'pdf' ]; then
  echo -e "${red}ERREUR:${clear} Le fichier de sortie doit être un pdf"
  exit 1
fi

# ### Vérifie si l'input est bien un dossier
# if [ ! -d $1 ]; then
#   echo -e "${red}ERREUR:${clear} \"${1}\" n'est pas un dossier :/"
#   exit 1
# fi

### Tout pour le style
figlet scanickel

### Crée un dossier temporaire pour les fichiers intermédiaires
mkdir -p /tmp/scanickel

###################### PRÉPARATION #######################
echo -ne "ACTIVER LE MODE AVANCÉ (o/N) ? -> "
read -r avance

case $avance in

  o | O)
    echo -e "${magenta}----- MODE AVANCÉ -----${clear}"

    ### Choix du mode selon le type de document scanné
    echo -ne "
  TRAITEMENT GLOBAL DES PAGES?

    1) Niveaux de gris (DÉFAUT)
    2) Couleur

--> "
    read -r mode

    ### Syntaxe bizarre pour définir les variables
    options_sup IMAGEMAGICK IM_BONUS
    options_sup IMG2PDF IMG_BONUS
    options_sup OCRMYPDF PDF_BONUS
    options_sup TESSERACT TES_BONUS

    ;;
  n | N | "")
    echo -e "${magenta}----- MODE BASIQUE -----${clear}"
    mode=""
    ;;
  *)
    fn_fail
    ;;
esac

###################### ÉXÉCUTION #######################

#-----------#
# NETTOYAGE #
#-----------#

case $mode in
1 | "")
  echo -ne "\nNETTOYAGE…"
  # convert ${1}* \
  #               -colorspace Gray \
  #               -type grayscale \
  #               -level 25%,73% \
  #               -contrast-stretch 0 \
  #               -normalize \
  #               -unsharp 10x10 \
  #               /tmp/scanickel/%04d_clean.jpg

  ;;
2)
  echo -ne "\nNETTOYAGE…"
  # convert ${1}* \
    #               -sigmoidal-contrast 10,50% \
    #               -normalize \
    #               -unsharp 10x10 \
    #               /tmp/scanickel/%04d_clean.jpg
  ;;
*)
  fn_fail
  ;;
esac

echo -e "\t${green}TERMINÉ :)${clear}"

#-----------#
# RECADRAGE #
#-----------#

# fichiers=($(ls /tmp/scanickel | grep _clean))

# Définit une page de gauche/de droite type pour appliquer le crop à toutes les autres images ensuite
PGAUCHE="/tmp/scanickel/0002_clean.jpg"
PDROITE="/tmp/scanickel/0003_clean.jpg"

# On vérifie si les pages existent pour s'en servir de gabarit
if [ -f $PGAUCHE ] && [ -f $PDROITE ]; then
  # Récupère le nom de base du fichier et l'extension
  Ginname="${PGAUCHE%.*}"
  Dinname="${PDROITE%.*}"

  echo -ne "MESURAGE…"

  # Récupère infos sur la page de gauche
  convert $PGAUCHE +repage -scale x1! -bordercolor black -border 1 -fuzz 30% -trim ${Ginname}_tmp1.jpg
  Gwidth=`convert ${Ginname}_tmp1.jpg -format "%w" info:`
  Gxoff=`convert ${Ginname}_tmp1.jpg -format "%O" info: | cut -d+ -f2`
  convert $PGAUCHE +repage -scale 1x! -bordercolor black -border 1 -fuzz 60% -trim ${Ginname}_tmp2.jpg
  Gheight=`convert ${Ginname}_tmp2.jpg -format "%h" info:`
  Gyoff=`convert ${Ginname}_tmp2.jpg -format "%O" info: | cut -d+ -f3`

  # Récupère infos sur la page de droite
  convert $PDROITE +repage -scale x1! -bordercolor black -border 1 -fuzz 30% -trim ${Diname}_tmp1.jpg
  Dwidth=`convert ${Diname}_tmp1.jpg -format "%w" info:`
  Dxoff=`convert ${Diname}_tmp1.jpg -format "%O" info: | cut -d+ -f2`
  convert $PDROITE +repage -scale 1x! -bordercolor black -border 1 -fuzz 60% -trim ${Diname}_tmp2.jpg
  Dheight=`convert ${Diname}_tmp2.jpg -format "%h" info:`
  Dyoff=`convert ${Diname}_tmp2.jpg -format "%O" info: | cut -d+ -f3`

  echo -e "\t${green}TERMINÉ :°${clear}"

  echo -ne "RECADRAGE…"

  # Recadre toutes les pages de gauche avec paramètres de la 1ère page de gauche
  # convert -set filename:name '%f' \
  #         /tmp/scanickel/???[02468]_clean.jpg \
  #         -crop ${Gwidth}x${Gheight}+${Gxoff}+${Gyoff} \
  #         +repage /tmp/scanickel/%[filename:name]_crop.jpg

  # Recadre toutes les pages de droite avec paramètres de la 1ère page de droite
  # convert -set filename:name '%f' \
  #         /tmp/scanickel/???[13579]_clean.jpg \
  #         -crop ${Dwidth}x${Dheight}+${Dxoff}+${Dyoff} \
  #         +repage /tmp/scanickel/%[filename:name]_crop.jpg

  echo -e "\t${green}TERMINÉ :O${clear}"

else
  echo -e "${red}ERREUR:${clear} Aucun fichier trouvé dans le dossier /tmp/scanickel :("
  exit 1
fi

# CONVERSION PDF
echo "PDFAGE…"
# RAJOUTER --clean pour utiliser unpaper (avec des fichiers plus propres)
# img2pdf /tmp/scanickel/*_crop.jpg | ocrmypdf - --deskew \
#                                                --remove-vectors \
#                                                --sidecar \
#                                                --optimize 2 \
#                                                ${TES_BONUS} \
#                                                ${2}

# NETTOYAGE DES FICHIERS TEMPORAIRES
rm -rf /tmp/scanickel/*

# scanickel

À partir d'un dossier de scans d'un livre, crée un PDF.

Plusieurs modes selon le type de document scanné : texte seulement, images seulement ou alors un mix des deux.

## Dépendences
* ImageMagick
* unpaper
* img2pdf
* tesseract (+ packs de langue)
* OCRMyPDF

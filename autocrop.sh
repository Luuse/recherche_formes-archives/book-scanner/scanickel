#!/bin/bash

# Récupère le nom de base du fichier et l'extension
inname="${1##*/}"
ext="${1##*.}"

# Recadre l'image (à peu près)
convert $1 +repage -scale x1! -bordercolor black -border 1 -fuzz 30% -trim ${inname}_tmp1.png
width=`convert ${inname}_tmp1.png -format "%w" info:`
xoff=`convert ${inname}_tmp1.png -format "%O" info: | cut -d+ -f2`

convert $1 +repage -scale 1x! -bordercolor black -border 1 -fuzz 60% -trim ${inname}_tmp2.png
height=`convert ${inname}_tmp2.png -format "%h" info:`
yoff=`convert ${inname}_tmp2.png -format "%O" info: | cut -d+ -f3`

convert $1 -crop ${width}x${height}+${xoff}+${yoff} +repage ${inname}_crop_deskew.jpg

# Redresse l'image (peut-être pas nécessaire si les appareils photos sont bien calibrés/alignés)
convert -set option:deskew:auto-crop true -deskew 40% ${inname}_crop_deskew.jpg ${inname}_crop_deskew.jpg

rm ${inname}_tmp*.png
